package edu.my;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Alex on 12.11.2016.
 */
public class Decoder {

    private String fileName;
    private List<String> Alphabet;
    private Double lettersAmount;
    private Map<String, Double> lettersOccurence;
    private Map<String, Double> frequencies;

    public Decoder(String fileName) {
        
        lettersAmount = Double.valueOf(0);

        this.fileName = fileName;

        lettersOccurence = new HashMap<>();
        
        String abc[] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
                "t", "u", "v", "w", "x", "y", "z"};

        Alphabet = new ArrayList<>(Arrays.asList(abc));
        
        for (int i = 0; i < Alphabet.size(); i++) {
            lettersOccurence.put(Alphabet.get(i), Double.valueOf(0));
        }

        Double freq[] = { 0.00065, 0.0011 , 0.0019, 0.00515, 0.01015, 0.014, 0.01585, 0.01775, 0.02055, 0.02195, 0.0232,
        0.02495, 0.0267, 0.02845, 0.03615, 0.04715, 0.05205, 0.0545, 0.0580, 0.06355, 0.0715, 0.0753, 0.0818, 0.0873, 0.10145, Double.MAX_VALUE };

        String freqAbc[] = {"q", "z", "x", "j", "v", "k", "p", "b", "f", "c", "y", "m", "g", "u", "w", "l", "d", "r", "s",
        "h", "i", "n", "o", "a", "t", "e"};

        frequencies = new HashMap<>();

        for (int i = 0; i < freqAbc.length; i++) {
            frequencies.put(freqAbc[i], freq[i]);
        }
        
    }

    public void getStatistics() {
        int c;
        String currentSymbol;
        try (FileReader fr = new FileReader(fileName)) {
            while((c = fr.read()) != -1) {
                currentSymbol = Character.toString((char)c).toLowerCase();
                if(Alphabet.contains(currentSymbol)) {
                    lettersAmount++;
                    Double currentLetterOccurence = lettersOccurence.get(currentSymbol);
                    currentLetterOccurence++;
                    lettersOccurence.replace(currentSymbol, currentLetterOccurence);
                }

            }
        } catch(IOException e) {
            System.out.println("I/O Error: " + e);
        }


        for (Map.Entry<String, Double> pair : lettersOccurence.entrySet()) {
            Double letterFrequency = pair.getValue()/lettersAmount;
            pair.setValue(letterFrequency);
        }


    }


    public void decrypt() {
        try (FileReader fr = new FileReader(fileName)) {
            FileWriter fw = new FileWriter("decrypted.txt");
            int c;
            while((c = fr.read()) != -1) {
                char encryptedSymbol = this.decryptSymbol((char)c);
                // Записать:
                try {
                    fw.write(encryptedSymbol);
                } catch (IOException e) {
                    System.out.println("I/O Error: " + e);
                }
            }

            fw.flush();

        } catch (IOException e) {
            System.out.println("I/O Error: " + e);
        }
    }

    private char decryptSymbol(char symbol) {
        if (Alphabet.contains(Character.toString(symbol).toLowerCase())) {
            String letter = Character.toString(symbol);
            if (Objects.equals(letter, letter.toLowerCase())) {
                if (lettersOccurence.get(letter) < frequencies.get("q")) return 'q';
                else if (lettersOccurence.get(letter) < frequencies.get("z")) return 'z';
                else if (lettersOccurence.get(letter) < frequencies.get("x")) return 'x';
                else if (lettersOccurence.get(letter) < frequencies.get("j")) return 'j';
                else if (lettersOccurence.get(letter) < frequencies.get("v")) return 'v';
                else if (lettersOccurence.get(letter) < frequencies.get("k")) return 'k';
                else if (lettersOccurence.get(letter) < frequencies.get("p")) return 'p';
                else if (lettersOccurence.get(letter) < frequencies.get("b")) return 'b';
                else if (lettersOccurence.get(letter) < frequencies.get("f")) return 'f';
                else if (lettersOccurence.get(letter) < frequencies.get("c")) return 'c';
                else if (lettersOccurence.get(letter) < frequencies.get("y")) return 'y';
                else if (lettersOccurence.get(letter) < frequencies.get("m")) return 'm';
                else if (lettersOccurence.get(letter) < frequencies.get("g")) return 'g';
                else if (lettersOccurence.get(letter) < frequencies.get("u")) return 'u';
                else if (lettersOccurence.get(letter) < frequencies.get("w")) return 'w';
                else if (lettersOccurence.get(letter) < frequencies.get("l")) return 'l';
                else if (lettersOccurence.get(letter) < frequencies.get("d")) return 'd';
                else if (lettersOccurence.get(letter) < frequencies.get("r")) return 'r';
                else if (lettersOccurence.get(letter) < frequencies.get("s")) return 's';
                else if (lettersOccurence.get(letter) < frequencies.get("h")) return 'h';
                else if (lettersOccurence.get(letter) < frequencies.get("i")) return 'i';
                else if (lettersOccurence.get(letter) < frequencies.get("n")) return 'n';
                else if (lettersOccurence.get(letter) < frequencies.get("o")) return 'o';
                else if (lettersOccurence.get(letter) < frequencies.get("a")) return 'a';
                else if (lettersOccurence.get(letter) < frequencies.get("t")) return 't';
                else return 'e';
            } else {
                if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("q")) return 'Q';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("z")) return 'Z';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("x")) return 'X';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("j")) return 'J';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("v")) return 'V';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("k")) return 'K';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("p")) return 'P';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("b")) return 'B';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("f")) return 'F';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("c")) return 'C';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("y")) return 'Y';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("m")) return 'M';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("g")) return 'G';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("u")) return 'U';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("w")) return 'W';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("l")) return 'L';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("d")) return 'D';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("r")) return 'R';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("s")) return 'S';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("h")) return 'H';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("i")) return 'I';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("n")) return 'N';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("o")) return 'O';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("a")) return 'A';
                else if (lettersOccurence.get(letter.toLowerCase()) < frequencies.get("t")) return 'T';
                else return 'E';
            }

        }
        return symbol; // что угодно кроме букв латинского алфавита
    }

}
